package org.bitbucket.ascommlib.events
{
	import flash.events.Event;

	/**
	 * @author Learning
	 */
	public class NetEvent extends Event
	{
		public static const RESPONSE : String = "response";
		public var result : Object;

		public function NetEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
