package org.bitbucket.ascommlib.effects
{
	import flash.display.DisplayObject;

	/**
	 * @author learning
	 */
	public class EffectMap
	{
		private static var map : Object = {};

		public static function addEffect(target : DisplayObject, effectName : String, duration : Number, params : Object = null) : Function
		{
			var effect : Effect = new Effect(target, effectName, duration, params);
			if (!map[target.name])
			{
				map[target.name] = {"current":effect, "queue":[]};
			}
			else
			{
				(map[target.name]["queue"] as Array).push(effect);
			}
			return addEffect;
		}

		public static function getEffect(target : DisplayObject) : Effect
		{
			if (map[target.name] != null)
			{
				return map[target.name]["current"] as Effect;
			}
			else
			{
				return null;
			}
		}

		public static function shiftEffect(target : DisplayObject) : void
		{
			var queue : Array = map[target.name]["queue"] as Array;
			delete map[target.name]["current"];
			if (queue.length > 0)
			{
				map[target.name]["current"] = queue.shift();
			}
			else
			{
				delete map[target.name];
			}
		}

		public static function clearEffect(target : DisplayObject) : void
		{
			var queue : Array = map[target.name]["queue"] as Array;
			if (queue.length > 0)
			{
				delete map[target.name]["current"];
				map[target.name]["current"] = queue[queue.length - 1];
			}
		}

		public static function getQueueLength(target : DisplayObject) : Number
		{
			if (map[target.name] == null)
			{
				return 0;
			}
			else
			{
				return (map[target.name]["queue"] as Array).length;
			}
		}

		public static function addEffectIfNotExists(target : DisplayObject, effectName : String, duration : Number, params : Object = null) : Function
		{
			var exists : Object = map[target.name];
			if (exists != null)
			{
				var arr : Array = exists['queue'] as Array;
				for each (var effect:Effect in arr)
				{
					if (effect.name == effectName)
					{
						return addEffectIfNotExists;
					}
				}
			}
			addEffect(target, effectName, duration, params);
			return addEffectIfNotExists;
		}

		public static function replaceEffectIfExists(target : DisplayObject, effectName : String, duration : Number, params : Object = null) : Function
		{
			var exists : Object = map[target.name];
			if (exists != null)
			{
				var arr : Array = exists['queue'] as Array;
				for (var i : int = 0; i < arr.length; i++)
				{
					if (arr[i]['name'] == effectName)
					{
						delete arr[i];
						break;
					}
				}
			}
			addEffect(target, effectName, duration, params);
			return replaceEffectIfExists;
		}
	}
}
