package org.bitbucket.ascommlib.effects {
	import flash.filters.BitmapFilter;
	import flash.utils.Dictionary;
	import flash.display.DisplayObject;

	/**
	 * 动画
	 * @author Shawn Huang
	 */
	public class Animation {
		private static var mapping : Dictionary = new Dictionary();
		public static var frameRate : int = 30;

		/**
		 * 将一个对象包装成动画，如果该对象已经被包装过一次，返回先前的对象 不用的时候记得unwrap一下
		 * 如果需要重新包装一个对象，new 一个AnimateObject
		 * @author Shawn Huang
		 * @param target 要包装的对象
		 * @return 包装完的对象
		 */
		public static function wrap(target : DisplayObject) : AnimateObject {
			var ret : AnimateObject = mapping[target.name];
			if (ret == null) {
				ret = new AnimateObject(target);
				mapping[target.name] = ret;
			}
			return ret;
		}

		/**
		 * 释放对象包装
		 * @param target 被封装的对象
		 * @author Shawn Huang
		 */
		public static function unwrap(target : DisplayObject) : void {
			var ret : AnimateObject = mapping[target.name];
			if (ret != null) {
				ret.destroy();
				delete mapping[target.name];
			}
		}

		private var _totalFrames : int = 12;

		public function get totalFrames() : int {
			return this._totalFrames;
		}

		private var _processFrames : int = 0;

		public function get processFrames() : int {
			return this._processFrames;
		}

		private var params : Object = null;
		private var origin : Object = {};
		private var target : DisplayObject = null;
		public var before : Function;
		public var after : Function;
		public var running : Function;

		public function Animation(target : DisplayObject, params : Object, totalFrames : int) {
			this.target = target;
			this._totalFrames = totalFrames;
			this.params = params;
		}

		/**
		 * 执行一帧动画，返回已经执行的帧数
		 * @author Shawn Huang
		 * @return 已经执行的帧数
		 */
		public function processEffect() : int {
			if (this._processFrames++ >= this.totalFrames) return _processFrames;
			for (var key:String in this.params) {
				// var step : Number = params[key] - origin[key];
				// var change : Number = step / this._totalFrames;
				// var value : Number = this._processFrames == this.totalFrames ? params[key] : this.target[key] + change;
				// this.target[key] = value;
				if (key == 'filters') {
					// this.doFilter(this.params[key][0], this.params[key][1]);
					this.loopFilter(this.params[key]);
					continue;
				}
				this.change(this.target, key, this.origin[key], this.params[key]);
			}
			if (this.running != null) {
				this.running(this.target);
			}
			return this._processFrames;
		}

		private function change(obj : Object, key : String, origin : Number, value : Number) : void {
			var step : Number = value - origin;
			var change : Number = step / this._totalFrames;
			var changes : Number = this._processFrames == this.totalFrames ? value : obj[key] + change;
//		    if (key == 'x' || key == 'y') {
//				changes = Math.ceil(changes);
//			}
			obj[key] = changes;
		}

		public function begin() : void {
			for (var key:String in params) {
				if (key == 'filters') {
					this.storeOriginFilter(params[key] as Array);
					continue;
				}
				this.origin[key] = target[key];
				var val : String = params[key];
				if (val.indexOf('+=') == 0) {
					params[key] = target[key] + parseFloat(val.substr(2));
				} else if (val.indexOf('-=') == 0) {
					params[key] = target[key] - parseFloat(val.substr(2));
				}
			}
			if (this.before != null) {
				// trace("animation started");
				this.before(this.target);
			}
		}

		public function finish() : void {
			for (var key:String in this.params) {
				this.target[key] = this.params[key];
			}
			this._processFrames = this.totalFrames;
		}

		public function end() : void {
			if (this.after != null) {
				// trace("animation ends");
				this.after(this.target);
			}
		}

		private function loopFilter(filterParams : Array) : void {
			for each (var obj : Object in filterParams) {
				this.doFilter(obj[0], obj[1]);
			}
		}

		private function doFilter(filterClz : Class, obj : Object) : void {
			var filter : BitmapFilter = this.getOrCreateFilter(filterClz);
			// for (var f : Object in this.target.filters) {
			// if (f is filterClz) {
			// filter = f as BitmapFilter;
			// break;
			// }
			// }
			// if (filter == null) {
			// filter = new filterClz();
			// this.target.filters.push(filter);
			// }
			for (var key : String in obj) {
				this.change(filter, key, this.origin[filterClz + ''][key], obj[key]);
			}
		}

		private function storeOriginFilter(arr : Array) : void {
			for each (var obj : Object in arr) {
				var clz : Object = obj[0];
				var filter : BitmapFilter = this.getOrCreateFilter(clz as Class);
				var ps : Array = [];
				for (var key : String in obj[1]) {
					var val : String = obj[1][key];
					var value : Number = 0;
					if (val.indexOf('+=') == 0) {
						value = filter[key] + parseFloat(val.substr(2));
					} else if (val.indexOf('-=') == 0) {
						value = filter[key] - parseFloat(val.substr(2));
					}
					var p : Object = {};
					p[key] = value;
					ps.push(p);
				}
				this.origin[clz + ''] = ps;
			}
		}

		private function getOrCreateFilter(filterClz : Class) : BitmapFilter {
			var filter : BitmapFilter = null;
			for (var f : Object in this.target.filters) {
				if (f is filterClz) {
					return f as BitmapFilter;
				}
			}
			filter = new filterClz();
			this.target.filters.push(filter);
			return filter;
		}
	}
}
