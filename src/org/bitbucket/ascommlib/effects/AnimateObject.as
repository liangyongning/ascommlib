package org.bitbucket.ascommlib.effects {
    import flash.events.Event;
    import flash.display.DisplayObject;

    /**
     * @author Shawn Huang
     */
    public class AnimateObject {
        private var queue : Array = [];
        private var currentEffect : Animation;
        private var stoped : Boolean = false;
        private var _target : DisplayObject;
        private var listening:Boolean = false;

        public function get target() : DisplayObject {
            return this._target;
        }

        public function AnimateObject(target : DisplayObject) {
            this._target = target;
        }
        
        /**
         * 创建一个动画加入队列
         * @param target 需要执行动画的对象
         * @param params 变化的目标参数 比如{width: 100}
         * @param frames 动画持续帧数 默认12帧(0.5秒)
         * @param after 动画开始前执行的回调 暂时没有传回调参数
         * @param before 动画结束后执行的回调 暂时没有传回调参数
         * @return 返回自己  可以连续调用animate 所有效果会顺序执行
         */
        public function animate(params:Object, frames:int = 15, after:Function = null, before:Function = null, running : Function = null):AnimateObject {
            var effect:Animation = new Animation(this.target, params, frames);
            effect.before = before;
            effect.after = after;
            effect.running = running;
            this.queue.push(effect);
            this.doNext();
            return this;
        }

        /**
         * 停止并且丢弃正在执行的动画，不影响队列中的动画。
         * 可以使用continue继续执行队列
         */
        public function stop() : AnimateObject {
            this.stoped = true;
            this.currentEffect = null;
            return this;
        }

        /**
         * 暂停当前动画，可以使用resume继续
         */
        public function pause() : AnimateObject {
            this.stoped = true;
            return this;
        }

        /**
         * 继续执行动画，只有在pause或者stop的时候才有效
         */
        public function resume() : AnimateObject {
            this.stoped = false;
            return this;
        }
        
        /**
         * 清除动画队列，不影响正在执行的动画效果
         */
        public function clearQueue() : AnimateObject {
            this.queue.length = 0;
            return this;
        }
        
        /**
         * 停止当前动作，清除当前和队列中的所有动画
         * 目标对象停留在当前状态
         */
        public function clean() : AnimateObject {
            return this.stop().clearQueue().resume();
        }
        
        /**
         * 将动画耗时变为1帧 快速结束掉
         */
        public function quickFinish() : AnimateObject {
            if (this.currentEffect != null) {
                this.currentEffect.finish();
            }
            return this;
        }

        [Destroy]
        public function destroy() : void {
            this.stopListen();
            this._target = null;
            this.queue.length = 0;
        }

        private function startListen() : void {
            if (!this.listening) {
                this.target.addEventListener(Event.ENTER_FRAME, this.doEffect);
                this.listening = true;
            }
        }
        
        private function stopListen() : void {
            if (this.listening) {
                this.target.removeEventListener(Event.ENTER_FRAME, this.doEffect);
                this.listening = false;
			}
		}
        private function doNext(force:Boolean = false) : void {
            if (force || this.currentEffect == null) this.currentEffect = this.queue.shift() as Animation;
            if (this.currentEffect) {
                this.startListen();
			} else {
                this.stopListen();
			}
        }

        private function doEffect(event:Event) : void {
            if (!this.stoped && this.currentEffect != null) {
                if (this.currentEffect.processFrames == 0) {
                    this.currentEffect.begin();
                }
                var processed : int = this.currentEffect.processEffect();
                if (processed >= this.currentEffect.totalFrames) {
                    this.currentEffect.end();
                    this.doNext(true);
                }
            }
        }
    }
}
