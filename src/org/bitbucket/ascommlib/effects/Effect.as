package org.bitbucket.ascommlib.effects
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.geom.Point;

	/**
	 * @author Learning
	 */
	public class Effect
	{
		public var target : DisplayObject;
		public var name : String;
		public var duration : Number;
		public var params : Object;
		public var origin : Point;
		public var running : Boolean = false;
		private static const FADE_IN : String = "fade in";
		private static const FADE_OUT : String = "fade out";
		private static const MOVE_TO : String = "move to";

		// private static var fadeInDuration : Number;
		// private static var fadeOutDuration : Number;
		public function Effect(target : DisplayObject, name : String, duration : Number, params : Object = null)
		{
			this.target = target;
			this.name = name;
			this.duration = duration;
			this.params = params;
		}

		public static function fadeIn(target : DisplayObject, duration : Number = 500, isBreak : Boolean = true) : Function
		{
			EffectMap.addEffect(target, FADE_IN, duration);
			startEffect(target, isBreak);
			// target.alpha = 0;
			// fadeInDuration = duration;
			// target.addEventListener(Event.ENTER_FRAME, fadeInHandler);
			return fadeIn;
		}

		private static function startEffect(target : DisplayObject, isBreak : Boolean = false) : void
		{
			if (isBreak && EffectMap.getEffect(target).running && EffectMap.getQueueLength(target) > 0)
			{
				EffectMap.clearEffect(target);
			}
			if (target.hasEventListener(Event.ENTER_FRAME))
			{
				try
				{
					target.removeEventListener(Event.ENTER_FRAME, effectHandler);
				}
				catch(e : Error)
				{
				}
			}
			EffectMap.getEffect(target).origin = new Point(target.x, target.y);
			target.addEventListener(Event.ENTER_FRAME, effectHandler);
		}

		private static function effectHandler(event : Event) : void
		{
			var target : DisplayObject = event.target as DisplayObject;
			var effect : Effect = EffectMap.getEffect(target);
			if (effect != null)
			{
				effect.running = true;
				switch(effect.name)
				{
					case FADE_IN:
						if (target.alpha >= 1)
						{
							EffectMap.shiftEffect(target);
						}
						else
						{
							target.alpha += 1 / ((effect.duration / 1000) * target.stage.frameRate);
						}
						break;
					case FADE_OUT:
						if (target.alpha <= 0)
						{
							EffectMap.shiftEffect(target);
						}
						else
						{
							target.alpha -= 1 / ((effect.duration / 1000) * target.stage.frameRate);
						}
						break;
					case MOVE_TO:
						var condx : Boolean;
						var condy : Boolean;
						var des : Point = effect.params as Point;
						if (effect.origin.x > des.x)
						{
							condx = (target.x <= des.x);
						}
						else if (effect.origin.x < des.x)
						{
							condx = (target.x >= des.x);
						}
						else
						{
							condx = true;
						}
						if (effect.origin.y > des.y)
						{
							condy = (target.y <= des.y);
						}
						else if (effect.origin.y < des.y)
						{
							condy = (target.y >= des.y);
						}
						else
						{
							condy = true;
						}
						if (!condx)
						{
							target.x += (des.x - effect.origin.x) / ((effect.duration / 1000) * target.stage.frameRate);
						}
						if (!condy)
						{
							target.y += (des.y - effect.origin.y) / ((effect.duration / 1000) * target.stage.frameRate);
						}
						if (condx && condy)
						{
							target.x = des.x;
							target.y = des.y;
							EffectMap.shiftEffect(target);
						}
						break;
				}
			}
			else
			{
				target.removeEventListener(Event.ENTER_FRAME, effectHandler);
				var nextEffect : Effect = EffectMap.getEffect(target);
				if (nextEffect != null)
				{
					startEffect(target);
				}
			}
		}

		// private static function fadeInHandler(event : Event) : void
		// {
		// var target : DisplayObject = event.target as DisplayObject;
		// if (target.alpha >= 1)
		// {
		// target.removeEventListener(Event.ENTER_FRAME, fadeInHandler);
		// }
		// else
		// {
		// target.alpha += 1 / ((fadeInDuration / 1000) * target.stage.frameRate);
		// }
		// }
		public static function fadeOut(target : DisplayObject, duration : Number = 500, isBreak : Boolean = true) : Function
		{
			EffectMap.addEffect(target, FADE_OUT, duration);
			startEffect(target, isBreak);
			// target.alpha = 1;
			// fadeOutDuration = duration;
			// target.addEventListener(Event.ENTER_FRAME, fadeOutHandler);
			return fadeOut;
		}

		// private static function fadeOutHandler(event : Event) : void
		// {
		// var target : DisplayObject = event.target as DisplayObject;
		// if (target.alpha <= 0)
		// {
		// target.removeEventListener(Event.ENTER_FRAME, fadeOutHandler);
		// }
		// else
		// {
		// target.alpha -= 1 / ((fadeInDuration / 1000) * target.stage.frameRate);
		// }
		// }
		public static function moveTo(target : DisplayObject, point : Point, duration : Number = 500, isBreak : Boolean = true) : Function
		{
			EffectMap.addEffect(target, MOVE_TO, duration, point);
			startEffect(target, isBreak);
			return moveTo;
		}
	}
}
