package org.bitbucket.ascommlib.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import org.bitbucket.ascommlib.events.NetEvent;

	/**
	 * @author Learning
	 */
	[Event(name="response", type="org.bitbucket.ascommlib.events.NetEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	public class HTTPRequest extends EventDispatcher {
		public static var jSessionId : String;
		private var _url : String;
		private var _method : String;
		private var urlRequest : URLRequest;
		private var urlLoader : URLLoader;
		private var urlVariables : URLVariables;
		public var noCache : Boolean = true;
		public var sCache : Boolean = false;

		public function HTTPRequest(url : String = null, method : String = URLRequestMethod.POST) {
			this.url = url;
			urlRequest = new URLRequest(this.url);
			urlRequest.method = method;
		}

		public function send(data : Object = null) : void {
			urlVariables = new URLVariables();
			for (var k : String in data) {
				urlVariables[k] = data[k];
			}
			if (noCache) {
				if (sCache) this.url = this.url += "&__no-cache__=" + Math.random();
				else urlVariables["__no-cache__"] = Math.random();
			}
			urlLoader = new URLLoader();
			urlRequest.data = urlVariables;
			urlLoader.addEventListener(Event.ACTIVATE, listener);
			urlLoader.addEventListener(Event.COMPLETE, listener);
			urlLoader.addEventListener(Event.DEACTIVATE, listener);
			urlLoader.addEventListener(Event.OPEN, listener);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, listener);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, listener);
			urlLoader.addEventListener(ProgressEvent.PROGRESS, listener);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, listener);
			urlLoader.load(urlRequest);
		}

		private function listener(event : Event) : void {
			if (event.type == Event.COMPLETE) {
				var e : NetEvent = new NetEvent(NetEvent.RESPONSE);
				e.result = urlLoader.data;
				dispatchEvent(e);
			} else {
				dispatchEvent(event);
			}
		}

		public function get url() : String {
			return _url;
		}

		public function set url(value : String) : void {
			if (jSessionId != null) {
				value += ";jsessionid=" + jSessionId;
			}
			_url = value;
			if (urlRequest != null) {
				urlRequest.url = url;
			}
		}

		public function get method() : String {
			return _method;
		}

		public function set method(value : String) : void {
			_method = value;
			urlRequest.method = method;
		}
	}
}
