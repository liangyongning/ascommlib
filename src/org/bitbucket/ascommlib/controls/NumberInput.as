package org.bitbucket.ascommlib.controls
{
        import flash.events.Event;
        import flash.events.KeyboardEvent;
        import flash.text.TextField;
        import flash.display.Sprite;
        import flash.text.AntiAliasType;
        import flash.text.TextFieldAutoSize;
        import flash.text.TextFieldType;
        import flash.text.TextFormat;

        /**
         * @author Learning
         */
        public class NumberInput extends Sprite
        {
                private var input : TextField;
                public var format : TextFormat;
                private var _max : int = 10;
                private var _min : int = 1;
                private var _size : Number = 12;
                private var _color : uint = 0x000000;
                private var _width : Number;
                private var _height : Number;

                // size.
                public function get size() : Number
                {
                        return _size;
                }

                public function set size(value : Number) : void
                {
                        _size = value;
                        redraw();
                }

                // color.
                public function get color() : uint
                {
                        return _color;
                }

                public function set color(value : uint) : void
                {
                        _color = value;
                        redraw();
                }

                public function NumberInput()
                {
                        input = new TextField();
                        input.autoSize = TextFieldAutoSize.LEFT;
                        input.antiAliasType = AntiAliasType.ADVANCED;
                        input.type = TextFieldType.INPUT;
                        input.restrict = "0-9";
                        input.addEventListener(KeyboardEvent.KEY_UP, check);
                        input.addEventListener(Event.ADDED_TO_STAGE, focusin);

                        format = new TextFormat();
                        format.font = "微软雅黑,宋体,_sans,simsun,Arial";
                        format.size = size;
                        format.color = color;

                        input.setTextFormat(format);

                        addChild(input);
                        width = 80;
                        drawBackground();
                }

                private function focusin(event : Event) : void
                {
                        input.stage.focus = input;
                        input.setSelection(0, int.MAX_VALUE);
                }

                private function check(event : KeyboardEvent) : void
                {
                        value = value;
                }

                public function set value(num : int) : void
                {
                        if (num > max)
                        {
                                num = max;
                        }
                        if (num < min)
                        {
                                num = min;
                        }
                        input.text = num.toString();
                        redraw();
                }

                public function get value() : int
                {
                        return parseInt(input.text);
                }

                public function set max(value : int) : void
                {
                        _max = value;
                }

                public function get max() : int
                {
                        return _max;
                }

                public function set min(value : int) : void
                {
                        _min = value;
                }

                public function get min() : int
                {
                        return _min;
                }

                override public function set width(value : Number) : void
                {
                        _width = value;
                        drawBackground();
                }

                override public function get width() : Number
                {
                        return _width;
                }

                override public function set height(value : Number) : void
                {
                        _height = value;
                        drawBackground();
                }

                override public function get height() : Number
                {
                        return _height;
                }

                private function redraw() : void
                {
                        format.size = size;
                        format.color = color;
                        input.setTextFormat(format);
                }

                private function drawBackground() : void
                {
                        graphics.clear();
                        graphics.beginFill(0xffffff, 1);
                        graphics.drawRect(0, 0, width, input.height);
                        graphics.endFill();
                        graphics.lineStyle(1, 0xCCCCCC);
                        graphics.lineTo(width, 0);
                        graphics.lineTo(width, input.height);
                        graphics.lineTo(0, input.height);
                        graphics.lineTo(0, 0);
                }
        }
}