package org.bitbucket.ascommlib.controls {
    import flash.events.MouseEvent;
    import flash.display.DisplayObject;
    import flash.display.Sprite;

    /**
     * @author Shawn Huang
     */
    public class AddibleButton extends Sprite {
        
        private var up : DisplayObject;
        private var over : DisplayObject;
        private var down : DisplayObject;
        private var disabled : DisplayObject;
		
		private var _upContainer : Sprite;
		private var _overContainer : Sprite;
		private var _downContainer : Sprite;
		private var _disabledContainer : Sprite;
        
        private var _overCallback : Function;
        private var _upCallback : Function;
        private var _downCallback : Function;
        private var _outCallback : Function;
        private var _clickCallback : Function;
        
        private var _enabled : Boolean = true;
        
        public function AddibleButton(upClass : Class, overClass : Class = null, downClass : Class = null, disabledClass : Class = null) {
			this._upContainer = new Sprite();
			this._overContainer = new Sprite();
			this._downContainer = new Sprite();
			this._disabledContainer = new Sprite();
			this.addChild(this._upContainer);
			this.addChild(this._overContainer);
			this.addChild(this._downContainer);
			this.addChild(this._disabledContainer);
            up = new upClass as DisplayObject;
            this._upContainer.addChild(this.up);
            if (overClass) {
                over = new overClass as DisplayObject;
                this._overContainer.addChild(this.over);
            }
            if (downClass) {
                down = new downClass as DisplayObject;
                this._downContainer.addChild(this.down);
            }
            if (disabledClass) {
                disabled = new disabledClass as DisplayObject;
                this._disabledContainer.addChild(this.disabled);
            }
            this.switchState(0);
            this.mouseChildren = false;
            this.buttonMode = true;
            this.addEventListener(MouseEvent.MOUSE_OVER, this.onMouseEvents);
            this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseEvents);
            this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvents);
            this.addEventListener(MouseEvent.CLICK, this.onMouseEvents);
            this.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvents);
        }
        
        public function onMouseEvents(event : MouseEvent) : void {
            if (!this._enabled) return;
            switch (event.type) {
                case MouseEvent.MOUSE_OVER : 
                    this.switchState(1);
                    this.call(this._overCallback, event);
                    break;
                case MouseEvent.MOUSE_DOWN:
                    this.switchState(2);
                    this.call(this._downCallback, event);
                    break;
                case MouseEvent.MOUSE_UP:
                    this.switchState(0);
                    this.call(this._upCallback, event);
                    break;
                case MouseEvent.CLICK:
                    this.call(this._clickCallback, event);
                    break;
                case MouseEvent.MOUSE_OUT:
                    this.switchState(0);
                    this.call(this._outCallback, event);
                    break;
            }
        }
        
        private function switchState(status : int) : void {
            var arr : Array = [this._upContainer, this._overContainer, this._downContainer, this._disabledContainer];
            for (var i : int = 0; i < arr.length; i++) {
                if (arr[i] && i == status) {
                    arr[i]['visible'] = true;
                } else if (arr[i]) {
                    arr[i]['visible'] = false;
                }
            }
        }
        
        private function call(func : Function, event : MouseEvent) : void {
            if (func == null) return;
            func(event);
        }
       
        public function set enabled(enabled : Boolean) : void {
            this._enabled = enabled;
            if (enabled) {
                this.switchState(0);
            } else {
                this.switchState(3);
            }
        }

        public function set overCallback(func : Function) : void {
            this._overCallback = func;
        }
        
        public function set donwCallback(func : Function) : void {
            this._downCallback = func;
        }
        public function set upCallback(func : Function) : void {
            this._upCallback = func;
        }
        public function set clickCallback(func : Function) : void {
            this._clickCallback = func;
        }
        public function set outCallback(func : Function) : void {
            this._outCallback = func;
        }
        
        public function get overCallback() : Function {
            return this._overCallback;
        }
        
        public function get donwCallback() : Function {
            return this._downCallback;
        }
        
        public function get upCallback() : Function {
            return this._upCallback;
        }
        
        public function get clickCallback() : Function {
            return this._clickCallback;
        }
        
        public function get outCallback() : Function {
            return this._outCallback;
        }
		
		public function get upContainer() : Sprite {
			return this._upContainer;
		}
		
		public function get overContainer() : Sprite {
			return this._overContainer;
		}
		
		public function get downContainner() : Sprite {
			return this._downContainer;
		}
		
		public function get disabledContainer() : Sprite {
			return this._disabledContainer;
		}
    }
}
