package org.bitbucket.ascommlib.controls
{
        import flash.text.AntiAliasType;
        import flash.text.TextField;
        import flash.text.TextFieldAutoSize;
        import flash.text.TextFormat;

        /**
         * @author Learning
         */
        public class Label extends TextField
        {
                public var format : TextFormat;
                private var _size : Number = 12;
                private var _color : uint = 0x000000;

                // size.
                public function get size() : Number
                {
                        return _size;
                }

                public function set size(value : Number) : void
                {
                        _size = value;
                        redraw();
                }

                // color.
                public function get color() : uint
                {
                        return _color;
                }

                public function set color(value : uint) : void
                {
                        _color = value;
                        redraw();
                }

                public function Label(text : String = '', html : Boolean = false)
                {
                        this.selectable = false;
                        this.autoSize = TextFieldAutoSize.LEFT;
                        this.antiAliasType = AntiAliasType.NORMAL;
                        // this.embedFonts = true;

                        format = new TextFormat();
//                        format.font = "微软雅黑,宋体,Hei,_sans,simsun,Arial";
                        format.font = "宋体,simsun,Hei";
                        format.size = size;
                        format.color = color;
                        if (html) {
                            this.htmlText = text;
                        } else {
                            this.text = text;
                        }
                        this.setTextFormat(format);
                }

                override public function set text(value : String) : void
                {
                        super.text = value || "";
                        redraw();
                }

                private function redraw() : void
                {
                        format.size = size;
                        format.color = color;
                        this.setTextFormat(format);
                }
        }
}