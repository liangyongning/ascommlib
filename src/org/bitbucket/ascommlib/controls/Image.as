package org.bitbucket.ascommlib.controls {
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;

	/**
	 * @author Learning
	 */
	public class Image extends Sprite {
		private var _source : String;
		private var request : URLRequest;
		private var loader : Loader;
		private var loaderContext:LoaderContext;
		private var loaded : Boolean = false;
		private var _width : Number = 0;
		private var _height : Number = 0;
		public var defaultImage : String;
		public var finishCallback : Function;
		private var loadingClass : Class;
		private var loading : DisplayObject;
		public var bitmapData:BitmapData;

		public function Image(source : String = null, loadingClass : Class = null) {
			this.loadingClass = loadingClass;
			loader = new Loader();
			loaderContext = new LoaderContext(true);
			request = new URLRequest();
			this.source = source;
		}

		public function get source() : String {
			return _source;
		}

		public function set source(value : String) : void {
			_source = value;
			if (numChildren > 0) {
				if (contains(loader)) removeChild(loader);
				loader = null;
				loader = new Loader();
				loaded = false;
				// width = 0;
				// height = 0;
			}
			if (source != null) {
				request.url = source;
				loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, progressHandler);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
				loader.load(request, loaderContext);
			}
		}

		private function progressHandler(event : ProgressEvent) : void {
			if (loadingClass != null) {
				if (loading == null) loading = new loadingClass as DisplayObject;
				if (!contains(loading)) addChild(loading);
				if (width > 0 && height > 0) {
					loading.x = (width - loading.width) / 2;
					loading.y = (height - loading.height) / 2;
				}
			}
		}

		private function completeHandler(event : Event) : void {
			if (loading != null && contains(loading)) {
				removeChild(loading);
				loading = null;
			}
			loaded = true;
			loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
			loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, completeHandler);
			if (width > 0 && height > 0) {
				loader.width = width;
				loader.height = height;
			}
			addChild(loader);
			if (this.finishCallback != null) {
				this.finishCallback(this);
			}
			var loaderInfo:LoaderInfo = event.target as LoaderInfo;
			this.bitmapData = new BitmapData(loaderInfo.width, loaderInfo.height);
			this.bitmapData.draw(loaderInfo.content);
			this.dispatchEvent(new Event(Event.COMPLETE));
		}

		private function errorHandler(event : IOErrorEvent) : void {
			trace("图片[" + source + "]加载失败");
			loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
			loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, completeHandler);
			if (defaultImage != null) this.source = defaultImage;
		}

		override public function get width() : Number {
			return _width;
		}

		override public function set width(value : Number) : void {
			_width = value;
			if (loaded && loader != null) {
				loader.width = width;
			}
		}

		override public function get height() : Number {
			return _height;
		}

		override public function set height(value : Number) : void {
			_height = value;
			if (loaded && loader != null) {
				loader.height = height;
			}
		}

		public function get originWidth() : int {
			if (this.loader != null) {
				return loader.width;
			}
			return 0;
		}

		public function get originHeight() : int {
			if (this.loader != null) {
				return this.loader.height;
			}
			return 0;
		}
	}
}