package org.bitbucket.ascommlib.controls {
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author learning
	 */
	public class CommonButton extends Sprite {
		// public class CommonButton extends SimpleButton {
		private var up : DisplayObject;
		private var over : DisplayObject;
		private var down : DisplayObject;
		private var disabled : DisplayObject;
		private var _active : Boolean = false;

		public function CommonButton(upClass : Class, overClass : Class, downClass : Class, disabledClass : Class = null) {
			this.up = new upClass as DisplayObject;
			this.over = new overClass as DisplayObject;
			this.down = new downClass as DisplayObject;
			if (disabledClass != null) this.disabled = new disabledClass as DisplayObject;
			else this.disabled = new upClass as DisplayObject;
			this.up.visible = true;
			this.over.visible = false;
			this.down.visible = false;
			this.disabled.visible = false;
			this.addChild(up);
			this.addChild(over);
			this.addChild(down);
			this.addChild(disabled);
			this.addEventListener(Event.ADDED_TO_STAGE, addedHandler);
			this.buttonMode = true;
		}

		private function addedHandler(event : Event) : void {
			this.addEventListener(Event.REMOVED_FROM_STAGE, removedHandler);
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
			this.addEventListener(MouseEvent.MOUSE_UP, mouseHandler);
		}

		private function removedHandler(event : Event) : void {
			this.addEventListener(Event.ADDED_TO_STAGE, addedHandler);
			this.mouseHandler(new MouseEvent(MouseEvent.MOUSE_OUT));
			this.removeEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			this.removeEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
			this.removeEventListener(MouseEvent.MOUSE_UP, mouseHandler);
		}

		private function mouseHandler(event : MouseEvent) : void {
			if(!enabled) return;
			switch(event.type) {
				case MouseEvent.MOUSE_OVER:
				case MouseEvent.MOUSE_UP:
					this.over.visible = true;
					this.up.visible = false;
					this.down.visible = false;
					this.disabled.visible = false;
					break;
				case MouseEvent.MOUSE_OUT:
					this.up.visible = true;
					this.over.visible = false;
					this.down.visible = false;
					this.disabled.visible = false;
					break;
				case MouseEvent.MOUSE_DOWN:
					this.down.visible = true;
					this.up.visible = false;
					this.over.visible = false;
					this.disabled.visible = false;
					break;
			}
		}

		public function get active() : Boolean {
			return _active;
		}

		public function set active(value : Boolean) : void {
			_active = value;
		}

		public function get enabled() : Boolean {
			return mouseEnabled;
		}

		public function set enabled(value : Boolean) : void {
			mouseEnabled = value;
			if (value) {
				this.up.visible = true;
				this.over.visible = false;
				this.down.visible = false;
				this.disabled.visible = false;
			} else {
				this.disabled.visible = true;
				this.up.visible = false;
				this.over.visible = false;
				this.down.visible = false;
			}
		}
		// up = new upClass as DisplayObject;
		// over = new overClass as DisplayObject;
		// down = new downClass as DisplayObject;
		// if (disabledClass) {
		// disabled = new disabledClass as DisplayObject;
		// }
		// super(up, over, down, up);
		// this.addEventListener(Event.ADDED_TO_STAGE, addedHandler);
		// }
		//
		// private function addedHandler(event : Event) : void {
		// super.dispatchEvent(new MouseEvent(MouseEvent.ROLL_OUT));
		// }
		//
		// public function set active(value : Boolean) : void {
		// _active = value;
		// if (value) {
		// upState = down;
		// overState = down;
		// hitTestState = down;
		// } else {
		// upState = up;
		// overState = over;
		// hitTestState = over;
		// }
		// }
		//
		// public function get active() : Boolean {
		// return _active;
		// }
		//
		// override public function set enabled(value : Boolean) : void {
		// super.enabled = value;
		// if (value) {
		// if (active) {
		// upState = down;
		// } else {
		// upState = up;
		// }
		// } else {
		// if (disabled) {
		// upState = disabled;
		// } else {
		// if (active) {
		// upState = down;
		// } else {
		// upState = up;
		// }
		// }
		// }
		// }
	}
}
