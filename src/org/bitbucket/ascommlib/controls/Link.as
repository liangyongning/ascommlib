package org.bitbucket.ascommlib.controls {
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	/**
	 * @author Learning
	 */
	public class Link extends Sprite {
		private var label : Label;
		/**
		 * 平时的背景颜色
		 */
		private var _normalBackgroundColor : uint = 0xFFFFFF;
		/**
		 * 平时的背景透明度
		 */
		private var _normalBackgroundAlpha : Number = 0;
		/**
		 * 平时的文字颜色
		 */
		private var _normalTextColor : uint = 0x0000FF;
		/**
		 * 激活时背景颜色
		 */
		private var _activeBackgroundColor : uint = 0xF0F000;
		/**
		 * 激活时背景透明度
		 */
		private var _activeBackgroundAlpha : Number = 1;
		/**
		 * 激活时文字颜色
		 */
		private var _activeTextColor : uint = 0x000000;
		private var _disabledTextColor : uint = 0xCCCCCC;
		private var _paddingTop : Number = 0;
		private var _paddingRight : Number = 0;
		private var _paddingBottom : Number = 0;
		private var _paddingLeft : Number = 0;
		private var _roundCorner : Number = 2;
		private var _enabled : Boolean = true;
		private var _activeUnderline:Boolean = false;

		public function Link() {
			super();

			mouseChildren = false;
			buttonMode = true;
			label = new Label();
			addChild(label);

			addEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			addEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			addEventListener(MouseEvent.CLICK, mouseHandler);
		}

		public function redraw() : void {
			label.x = paddingLeft;
			label.y = paddingTop;
			label.format.underline = false;
			label.color = normalTextColor;
			graphics.clear();
			graphics.beginFill(normalBackgroundColor, normalBackgroundAlpha);
			graphics.drawRoundRect(0, 0, label.width + paddingLeft + paddingRight, label.height + paddingTop + paddingBottom, roundCorner);
			graphics.endFill();
			enabled = enabled;
		}

		private function active() : void {
			label.format.underline = activeUnderline;
			label.color = activeTextColor;
			graphics.clear();
			graphics.beginFill(activeBackgroundColor, activeBackgroundAlpha);
			graphics.drawRoundRect(0, 0, label.width + paddingLeft + paddingRight, label.height + paddingTop + paddingBottom, roundCorner);
			graphics.endFill();
		}

		private function mouseHandler(event : MouseEvent) : void {
			if (!enabled) return;
			switch(event.type) {
				case MouseEvent.MOUSE_OVER:
					active();
					break;
				case MouseEvent.MOUSE_OUT:
					redraw();
					break;
			}
		}

		public function get text() : String {
			return label.text;
		}

		public function set text(value : String) : void {
			label.text = value;
			redraw();
		}

		public function get htmlText() : String {
			return label.htmlText;
		}

		public function set htmlText(value : String) : void {
			label.htmlText = value;
		}

		public function get normalBackgroundColor() : uint {
			return _normalBackgroundColor;
		}

		public function set normalBackgroundColor(value : uint) : void {
			_normalBackgroundColor = value;
			redraw();
		}

		public function get normalBackgroundAlpha() : Number {
			return _normalBackgroundAlpha;
		}

		public function set normalBackgroundAlpha(value : Number) : void {
			_normalBackgroundAlpha = value;
			redraw();
		}

		public function get normalTextColor() : uint {
			return _normalTextColor;
		}

		public function set normalTextColor(value : uint) : void {
			_normalTextColor = value;
			redraw();
		}

		public function get activeBackgroundColor() : uint {
			return _activeBackgroundColor;
		}

		public function set activeBackgroundColor(value : uint) : void {
			_activeBackgroundColor = value;
		}

		public function get activeBackgroundAlpha() : Number {
			return _activeBackgroundAlpha;
		}

		public function set activeBackgroundAlpha(value : Number) : void {
			_activeBackgroundAlpha = value;
		}

		public function get activeTextColor() : uint {
			return _activeTextColor;
		}

		public function set activeTextColor(value : uint) : void {
			_activeTextColor = value;
		}

		public function get paddingTop() : Number {
			return _paddingTop;
		}

		public function set paddingTop(value : Number) : void {
			_paddingTop = value;
			redraw();
		}

		public function get paddingRight() : Number {
			return _paddingRight;
		}

		public function set paddingRight(value : Number) : void {
			_paddingRight = value;
			redraw();
		}

		public function get paddingBottom() : Number {
			return _paddingBottom;
		}

		public function set paddingBottom(value : Number) : void {
			_paddingBottom = value;
			redraw();
		}

		public function get paddingLeft() : Number {
			return _paddingLeft;
		}

		public function set paddingLeft(value : Number) : void {
			_paddingLeft = value;
			redraw();
		}

		public function get roundCorner() : Number {
			return _roundCorner;
		}

		public function set roundCorner(value : Number) : void {
			_roundCorner = value;
			redraw();
		}

		public function set disabledTextColor(value : uint) : void {
			_disabledTextColor = value;
		}

		public function get disabledTextColor() : uint {
			return _disabledTextColor;
		}

		public function set enabled(value : Boolean) : void {
			_enabled = value;
			buttonMode = value;
			if (value) label.color = normalTextColor;
			else label.color = disabledTextColor;
		}

		public function get enabled() : Boolean {
			return _enabled;
		}

		public function set bold(value : Object) : void {
			label.format.bold = value;
			redraw();
		}

		public function get bold() : Object {
			return label.format.bold;
		}

		public function get activeUnderline():Boolean
		{
			return _activeUnderline;
		}

		public function set activeUnderline(value:Boolean):void
		{
			_activeUnderline = value;
		}
		
		public function set size(value:Number):void
		{
			label.size = value;
			redraw();
		}
		
		public function get size():Number
		{
			return label.size;
		}

	}
}