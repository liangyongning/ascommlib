package org.bitbucket.ascommlib.controls {
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextField;
	/**
	 * @author Shawn Huang
	 */
	public class TextLabel extends TextField {
		
		
		private var format : TextFormat;
		private var _autoInvoke : Boolean = true;
		
		public function TextLabel(text : String = null, isHtml : Boolean = false) {
			if (text && isHtml) {
				this.htmlText = text;
			} else if (text) {
				this.text = text;
			}
			
			this.selectable = false;
			
			this.format = new TextFormat();
			this.format.size = 12;
			this.defaultTextFormat = this.format;
			this.autoSize = TextFieldAutoSize.LEFT;
		}
		
		public function set color(c : Object) : void {
			this.format.color = c;
			this.invokeFormat(this.autoInvoke);
		}
		
		public function get color() : Object {
			return this.format.color;
		}
		
		public function set size(s : Object) : void {
			this.format.size = s;
			this.invokeFormat(this.autoInvoke);
		}
		
		public function get size() : Object {
			return this.format.size;
		}
		
		public function set align(align : String) : void {
			this.autoSize = align;
			this.invokeFormat(this.autoInvoke);
		}
		
		public function get align() : String {
			return this.autoSize;
		}
		
		public function set bold (b : Object) : void {
			this.format.bold = b;
			this.invokeFormat(this.autoInvoke);
		}
		
		public function get bold() : Object {
			return this.format.bold;
		}
		
		public function invokeFormat(force : Boolean = true) : void {
			if(!force) return;
			this.defaultTextFormat = this.format;
			this.text = this.text;
		}
		
		public function set autoInvoke(value : Boolean) : void {
			this._autoInvoke = value;
			if (value) {
				this.invokeFormat();
			}
		}
		
		public function get autoInvoke() : Boolean {
			return this._autoInvoke;
		}
		
		public function get font() : String {
			return this.format.font;
		}
		
		public function set font(value : String) : void {
			this.format.font = value;
			this.invokeFormat(this.autoInvoke);
		}
		
	}
}
